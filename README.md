# panda_teaching

![Panda in Gazebo](assets/panda-in-gazebo.png?raw=true "Panda in Gazebo")

#### Getting started

This package was tested with the provided [simulation workspace](https://git-st.inf.tu-dresden.de/ceti/ros-internal/panda_gazebo_workspace). So first you have to clone everything as described in [the README of the workspace](https://git-st.inf.tu-dresden.de/ceti/ros/panda_gazebo_workspace/-/blob/master/README.md). Secondly, you have to clone this repository into the `src` dirctory of the workspace.

#### Running the simulation

Build the catkin workspace and run the simulation with:
```
catkin build
source devel/setup.bash
roslaunch panda_teaching simulation.launch
```

#### Teaching poses

You can teach the (simulated) robot several poses which can be replayed in the order they where taught.

##### Steps to teach the robot poses

- Teach a new sequence of poses
    1. Drag the robot in rviz to the target destination.
    2. Plan and execute the trajectory using the rviz planning interface.
    3. Run `rostopic pub -1 panda_teaching/poseRecorder std_msgs/String 'add'`.
    4. Repeat steps 1 to 3 as desired.
- Replay the sequence
    1. Run `rostopic pub -1 panda_teaching/poseReplayer std_msgs/String 'replay'`.
- Reset the pose sequence
    1. Run `rostopic pub -1 panda_teaching/poseRecorder std_msgs/String 'clear'`.


