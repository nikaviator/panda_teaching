//
// Created by sebastian on 04.05.20.
//
#include <stdlib.h>

#include <ros/ros.h>
#include <tf2_ros/transform_listener.h>
#include <geometry_msgs/TransformStamped.h>
#include <std_msgs/String.h>

#include "panda_teaching/SavePose.h"
#include "panda_teaching/DeletePose.h"
#include "panda_teaching/ComputeError.h"
#include "panda_teaching/GetCollectedPoses.h"

// global variable. should be put in class
tf2_ros::Buffer tfBuffer;

void collect(ros::NodeHandle node_handle, geometry_msgs::TransformStamped t) {

    ros::ServiceClient client = node_handle.serviceClient<panda_teaching::SavePose>("SaveCollectedPose");
    panda_teaching::SavePose srv;

    srv.request.pose.position.x = t.transform.translation.x;
    srv.request.pose.position.y = t.transform.translation.y;
    srv.request.pose.position.z = t.transform.translation.z;

    srv.request.pose.orientation = t.transform.rotation;

    if (!client.call(srv)) {
        ROS_ERROR("Failed to call pose_storage_service.");
        return;
    }
}

void cleanUp(ros::NodeHandle &node_handle) {
    ros::ServiceClient client = node_handle.serviceClient<panda_teaching::DeletePose>("DeleteCollectedPoses");
    panda_teaching::DeletePose srv;

    if (!client.call(srv)) {
        ROS_ERROR("Failed to delete saved poses");
    }
}

void collectorCallback(const std_msgs::String::ConstPtr &msg, ros::NodeHandle &node_handle) {

    ROS_INFO("Retrieved new pose-message...");

    std::string sourceFrame;
    if (!node_handle.getParam("pose_element", sourceFrame)) {
        ROS_ERROR("Could not get string value for panda_teaching/pose_element from param server");
        return;
    }

    if (msg->data.compare("add") == 0) {
        ROS_INFO_STREAM("Recording new pose for " << sourceFrame << "...");
        try {
            geometry_msgs::TransformStamped transformStamped = tfBuffer.lookupTransform("world", sourceFrame,
                                                                                        ros::Time(0));

            std::cout << "--------------------------------" << std::endl;
            std::cout << "Added pose: " << transformStamped << std::endl;
            std::cout << "--------------------------------" << std::endl;
            collect(node_handle, transformStamped);
        }
        catch (tf2::TransformException &ex) {
            ROS_ERROR_STREAM("transform_error: " << ex.what() << std::endl);
            ros::Duration(1.0).sleep();
        }
    } else if (msg->data.compare("clear") == 0) {
        ROS_INFO_STREAM("Deleting saved poses...");
        cleanUp(node_handle);
    } else {
        ROS_ERROR_STREAM("pose_tf_listener received unknown panda_teaching/poseRecorder message " << msg->data);
    }
}

bool errorComputationCallback(panda_teaching::ComputeError::Request &req,
                   panda_teaching::ComputeError::Response &res, ros::NodeHandle &node_handle) {

    std::string sourceFrame;
    if (!node_handle.getParam("pose_element", sourceFrame)) {
        ROS_ERROR("Could not get string value for panda_teaching/pose_element from param server");
        return false;
    }

    try {
        geometry_msgs::TransformStamped transformStamped = tfBuffer.lookupTransform("world", sourceFrame,
                                                                                    ros::Time(0));
        int poseNumber = req.poseNumber;
        geometry_msgs::Pose currentTargetPose;

        ros::ServiceClient client = node_handle.serviceClient<panda_teaching::GetCollectedPoses>("GetCollectedPoses");
        panda_teaching::GetCollectedPoses srv;

        if (client.call(srv)) {
            currentTargetPose = srv.response.pose_array.poses[poseNumber];

            std::cout << "position-error (xyz): " << (std::abs(transformStamped.transform.translation.x) -std::abs( currentTargetPose.position.x)) << " -- "
                    << (std::abs(transformStamped.transform.translation.y) - std::abs(currentTargetPose.position.y)) << " -- "
                    << (std::abs(transformStamped.transform.translation.z) - std::abs(currentTargetPose.position.z)) << " -- " << std::endl;

            std::cout << "orientation (wxyz): " << (std::abs(transformStamped.transform.rotation.w) - std::abs(currentTargetPose.orientation.w)) << " -- "
                    << (std::abs(transformStamped.transform.rotation.x) - std::abs(currentTargetPose.orientation.x)) << " -- "
                    << (std::abs(transformStamped.transform.rotation.y) - std::abs(currentTargetPose.orientation.y)) << " -- "
                    << (std::abs(transformStamped.transform.rotation.z) - std::abs(currentTargetPose.orientation.z)) << " -- " << std::endl;
        } else {
            ROS_ERROR("Failed to fetch saved poses");
            return false;
        }

    }
    catch (tf2::TransformException &ex) {
        ROS_ERROR_STREAM("transform_error: " << ex.what() << std::endl);
        ros::Duration(1.0).sleep();
        return false;
    }
    return true;
}

int main(int argc, char **argv) {
    ros::init(argc, argv, "pose_tf_listener");
    ros::NodeHandle node_handle("panda_teaching");

    ros::Subscriber recorder_service = node_handle.subscribe<std_msgs::String>("poseRecorder", 1000,
                                                                  boost::bind(&collectorCallback, _1,
                                                                              boost::ref(node_handle)));

    ros::ServiceServer error_service = node_handle.advertiseService<panda_teaching::ComputeError::Request, panda_teaching::ComputeError::Response>("errorChecker",
                                        boost::bind(&errorComputationCallback, _1, _2, boost::ref(node_handle)));


    tf2_ros::TransformListener tfListener(tfBuffer);

    ros::Rate rate(10.0);

    ros::spin();
    return 0;
};
