#include "ros/ros.h"
#include "panda_teaching/GetCollectedPoses.h"
#include "panda_teaching/SavePose.h"
#include "panda_teaching/DeletePose.h"
#include <geometry_msgs/Pose.h>

std::vector<geometry_msgs::Pose> poses;

bool getCollectedPoses(panda_teaching::GetCollectedPoses::Request &req,
                       panda_teaching::GetCollectedPoses::Response &res) {

  res.pose_array.poses = poses;
  return true;
}

bool saveCollectedPose(panda_teaching::SavePose::Request &req,
                       panda_teaching::SavePose::Response &res) {

  poses.push_back(req.pose);
  return true;
}

bool deleteCollectedPoses(panda_teaching::DeletePose::Request &req,
                          panda_teaching::DeletePose::Response &res) {

  poses.clear();
  return true;
}

int main(int argc, char **argv) {

  ros::init(argc, argv, "pose_storage_service");
  ros::NodeHandle n("panda_teaching");

  ros::ServiceServer retrieve_service = n.advertiseService("GetCollectedPoses", getCollectedPoses);
  ros::ServiceServer save_service = n.advertiseService("SaveCollectedPose", saveCollectedPose);
  ros::ServiceServer delete_service = n.advertiseService("DeleteCollectedPoses", deleteCollectedPoses);

  ROS_INFO("Ready to collect.");
  ros::spin();

  return 0;
}
